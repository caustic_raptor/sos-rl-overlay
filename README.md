# SOS RL Overlay

This repo houses the files needed to produce a quick overlay. Aditionally, you will need a few more items to get the overlay working properly.

## Getting started

The following programs are needed in order for this to work properly. The first being Rocket League of course! You can download Rocket League through the Epic Games launcher on desktop.

Bakkesmod can be found in the top left of this site here: https://bakkesplugins.com/. SOS is not a verified plugin for bakkesmod so you can't download it through the plugins site listed, regardless you'll be able to download bakkesmod here.

Codename Covert created the SOS plugin and its wonderful. You can clone the SOS repo here: https://gitlab.com/bakkesplugins/sos    If you're curious as to how it all works, you can visit this reddit post, it helped me a lot: https://www.reddit.com/r/RocketLeagueEsports/comments/huszj4/how_to_make_your_own_custom_broadcast_overlay/

Install NodeJS: https://nodejs.org/en/   Node is how you'll run the ws-relay.js file to scrap for the game ticks. Its how the overlay actually gets the data from Rocket League.

## Running the overlay

The way I've done it thus far is by making a folder in the SOS folder titled "Overlays". I put all of the files in my repo in that folder. That way, whenever I edited the code, I didn't have to worry about searching for png files or whatever.

If you look at the CSS file, I made some comments in there for you to follow what adjusts what. I'm by no means a CSS, HTML or JS expert so please change the code to your liking. I'm also not a graphic designer so if you want to create your own scoreboard, boost tiles and player card in photoshop and us my code as the running engine, go for it. W3 schools has a good repository on CSS and how it all works together.

To actually make this work you need to follow these steps...

Step 1: Open Bakkesmod and get it ready to inject. It'll tell you when its waiting for Rocket League.
Step 2: Open the Epic Launcher & Run Rocket League
Step 3: While Rocket League is loading, go to the sos-ws-relay folder from the sos clone and open that up.
Step 4: Open Powershell or CMD in that folder and type the following:    node ws-relay.js
Step 5: Hit enter 3 times, SOS is now running.
Step 6: Create a browser source in streamlabs or whatever streaming software your using, navigate to overlay.html locally and select that file.
Step 7: Set up your private lobby and get ready for the game.
Step 8: When the game loads and you go into spectator, hit H a few times to drop the RL overlay.
Step 9: You might not have to do this, but sometimes I have to refresh the cache in the browser source, just click on the browser source in streamlabs and hit refresh.

That should be it. At least, that's how it works for me. I followed the tutorials on Reddit as well as looked up a few videos. They have a support discord as well which I can't find the invite for.

## Known bugs

If a players name is too long, it clips the boost gauge and the score in the playercard. That's something I plan on fixing but just don't have the time now.

## Planned upgrades to the repo

Over the summer once esports cools down a bit, I'm going to try and make the overlay more dynamic and actually have a graphic designer overhaul it. It *works* and that's all that mattered to me.
